require 'open-uri'
require "prawn"
require 'fileutils'

puts "Get Url"
url = File.open("url.txt").read

puts "Scan Website"
images = open(url).read.scan(/"(.*?)"/im)
    .map { |i| i[0].to_s }
    .select { |i| i=~/(.jpg|.png|.jpeg|.gif)/im }
    .reject {|i| ['.jpg', '.gif', '.png', '.jpeg'].include?(i) }
    .map do |img|
        img =~ /^http/i ? img : URI.join(url, img)
end

puts "Create Tmp Folder"
FileUtils.mkdir_p 'tmp'

t = 0
images.each do |image|
    puts "Download: " + t.to_s + "/" + images.count.to_s  
    if (t > 7)
    open(image) do |im|
            File.open("./tmp/" + t.to_s + ".jpg", "wb") do |file|
                file.write(im.read)
            end
        end
    end
    t = t + 1
end
puts "Download Complete!"

i = 0
puts "Create Pdf File"
Prawn::Document.generate(url.split('/').last + '.pdf', :page_size => "A4") do |pdf|
    images.each do |image|
        write = i + 1
        puts "Add Page To Pdf: " + write.to_s + "/" + images.count.to_s
        if (i > 7)
            unless(i == 58)
                open(image) do |im|
                    pdf.image "./tmp/" + i.to_s + ".jpg", :scale => 0.25
                end
            end
        end
        i = i + 1
    end
end
puts "Pdf Complete!"
FileUtils.rm_rf('./tmp') 
puts "Remove Tmp Folder"